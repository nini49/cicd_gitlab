#!/usr/bin/env python
import yaml
import click

def creer_inventory(serveur, nombre_serveur):
        with open("00_inventory.yml", "r") as f:
                dico = yaml.safe_load(f)

        dico_serveur = {}
        for x in range(1, int(nombre_serveur) + 1):
                nom_serveur = serveur + str(x)
                dico_serveur.update({nom_serveur: None})
                dico["all"]["children"]["all_server"]["children"]["server"]["hosts"] = dico_serveur
                print("Le serveur", nom_serveur, "a été ajouté a l'inventory.")

        with open("00_inventory.yml", "w") as f:
                yaml.dump(dico,f)

@click.command()
@click.argument("serveur")
@click.argument("nombre_serveur")
def main(serveur, nombre_serveur):
       creer_inventory(serveur, nombre_serveur)


if __name__ == "__main__":
    main()
