#!/bin/bash

echo "Lancement du serveur vagrant pour le runner gitlab-ci"
cd /home/$USER/gitlab/cicd_gitlab/runner/serveur_runner \
&& vagrant box update \
&& vagrant up \
&& echo Creation des serveurs terminés
