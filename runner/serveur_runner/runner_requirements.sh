#!/bin/bash

echo "Mise à jour ubuntu"
apt -y update \
&& apt -y upgrade

echo "Passer le clavier en azerty"
sudo loadkeys fr

echo "Installation de docker"
sudo apt install -y docker.io \
&& sudo usermod -aG docker vagrant

echo "Installation pip"
sudo apt install -y pip3

echo "Installation ansible"
pip3 install ansible

echo "Installation requirement ssh"
sudo apt install -y sshpass

echo "Installation runner"
docker run -d --name gitlab-runner \
--restart always \
-v /var/run/docker.sock:/var/run/docker.sock \
-v /data/gitlab-runner:/etc/gitlab-runner gitlab/gitlab-runner:latest

echo "-------------------------------------------------------------------"
echo "Fin des installation du fichier runner_requirements.sh"
echo "-------------------------------------------------------------------"