#!/bin/bash

echo "Suppression du serveur vagrant pour runner gitlab-ci"
cd /home/$USER/gitlab/cicd_gitlab/runner/serveur_runner \
&& vagrant destroy \
&& echo Suppression des serveurs terminés
