#!/bin/bash

adresse_ip_du_serveur=10.0.0.1
username_du_compte_qui_est_dans_le_serveur=vagrant
mot_de_passe_du_username=vagrant
mot_de_passe_user_ssh=motdepasse
nombre_serveur=1

nom_variable_adresse_ip_du_serveur=${adresse_ip_du_serveur//./_}

echo "Creation de la variable 'NOMBRE_SERVEUR' pour le 'Vagrantfile'"
export NOMBRE_SERVEUR=$nombre_serveur

echo "Creation de la cle ssh pc_portable (local)"
ssh-keygen -q -t ecdsa -N '' -f /home/$USER/.ssh/id_ecdsa_local -b 521
cat /home/$USER/.ssh/id_ecdsa_local.pub | tee /home/$USER/.ssh/authorized_keys

echo "Lancement du serveur vagrant pour le deploiement"
cd /home/$USER/gitlab/cicd_gitlab/serveur_deploiement \
&& vagrant box update \
&& vagrant up \
&& echo Creation des serveurs terminés

START=1
END=$nombre_serveur


echo "Démarrer l'agent ssh"
eval $(ssh-agent -s)

for ((c=$START; c<=$END; c++))
    do 
        echo "Démarrage de l'installation du serveur deploiement n°$c"
        echo "-------------------------------------------------------------------"

        echo "Creer dossier pour cle prive"
        mkdir -p /home/$USER/gitlab/cicd_gitlab/serveur_deploiement/cle_ssh/deploiement$c

        echo "Supprimer les anciennes clées ssh du serveur $adresse_ip_du_serveur$c"
        ssh-keygen -f "/home/$USER/.ssh/known_hosts" -R "$adresse_ip_du_serveur$c"

        echo "Alimenter agent ssh - Ajout de la cle prive ssh $adresse_ip_du_serveur$c"
        ssh-add /home/$USER/gitlab/cicd_gitlab/serveur_deploiement/.vagrant/machines/deploiement$c/virtualbox/private_key

        echo "Recuperer la cle prive ssh du user de deploiement"
        scp $username_du_compte_qui_est_dans_le_serveur@$adresse_ip_du_serveur$c:/home/vagrant/.ssh/id_ecdsa_deploy /home/$USER/gitlab/cicd_gitlab/serveur_deploiement/cle_ssh/deploiement$c/id_ecdsa_deploy


#         # echo "Test de la connexion SSH avec le serveur de deploiement" # Si la connexion SSH ne fonctionne pas, Ansible ne va pas fonctionner. Ansible a besoin d'une connexion SSH pour fonctionner
#         # sshpass -p "" ssh -o "StrictHostKeyChecking no" $username_du_compte_qui_est_dans_le_serveur@$adresse_ip_du_serveur$c

#         echo "Enregistrer dans l'agent ssh la clé privé du serveur $adresse_ip_du_serveur$c"
#         ssh-add ~/gitlab/cicd_gitlab/serveur_deploiement/.vagrant/machines/deploiement$c/virtualbox/private_key

#         echo "Test de connexion avec le serveur de deploiement"
#         sshpass -p "$mot_de_passe_du_username" ansible -i "$adresse_ip_du_serveur$c," all -u $username_du_compte_qui_est_dans_le_serveur -k -m ping

#         ssh-add -l

#         echo "Creer clé ssh"
#         mkdir -p /home/$USER/gitlab/cicd_gitlab/serveur_deploiement/cle_ssh/deploiement$c
#         ssh-keygen -q -t ecdsa -N '' -f /home/$USER/gitlab/cicd_gitlab/serveur_deploiement/cle_ssh/deploiement$c/id_ecdsa_$nom_variable_adresse_ip_du_serveur$c -b 521
#         # cat /home/$USER/.ssh/id_ecdsa_$nom_variable_adresse_ip_du_serveur$c.pub | tee /home/$USER/.ssh/authorized_keys
#         # # chown -R $nom_user:$nom_user /home/$nom_user/.ssh
#         # sshpass -p motdepasse ssh-copy-id -i /home/$USER/.ssh/id_ecdsa_$nom_variable_adresse_ip_du_serveur -o StrictHostKeyChecking=no deploy@$adresse_ip_du_serveur

        echo "Ajouter variable - clé privé ssh dans gitlab"
        glab variable set SSH_PRIVATE_$nom_variable_adresse_ip_du_serveur$c < /home/$USER/gitlab/cicd_gitlab/serveur_deploiement/cle_ssh/deploiement$c/id_ecdsa_deploy

        echo "Ajouter variable - user ssh dans gitlab"
        glab variable set SSH_USER_$nom_variable_adresse_ip_du_serveur$c $adresse_ip_du_serveur$c

        # echo "Ajouter variable - user ssh dans gitlab"
        # glab variable set SSH_PSW_$nom_variable_adresse_ip_du_serveur$c $mot_de_passe_user_ssh

#         echo "-------------------------------------------------------------------"
#         echo "Fin du lancement du fichier lancement_serveur_deploiement.sh"
#         echo "-------------------------------------------------------------------"
    done

