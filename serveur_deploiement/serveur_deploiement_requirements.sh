#!/bin/bash

# echo "Mise à jour ubuntu"
# apt -y update \
# && apt -y upgrade

nom_user=deploy

echo "Passer le clavier en azerty"
sudo loadkeys fr

echo "Desactiver le fingerprint"
echo "Host *" > /home/$nom_user/.ssh/config \
&& echo "    StrictHostKeyChecking no" >> /home/$nom_user/.ssh/config

echo "Creer user '$nom_user' pour deploiement"
useradd $nom_user --home /home/$nom_user/ --create-home --shell /bin/bash
usermod --password $(echo motdepasse | openssl passwd -1 -stdin) $nom_user

echo "Creer clé ssh pour $nom_user pour deploiement"
mkdir /home/$nom_user/.ssh
ssh-keygen -q -t ecdsa -N '' -f /home/$nom_user/.ssh/id_ecdsa_deploy -b 521
cat /home/$nom_user/.ssh/id_ecdsa_deploy.pub | tee /home/$nom_user/.ssh/authorized_keys
chown -R $nom_user:$nom_user /home/$nom_user/.ssh

echo "Permettre a $nom_user d'utiliser la commande sudo"
usermod -aG sudo $nom_user

echo "Copier la cle prive de $nom_user dans le dossier vagrant"
cp /home/$nom_user/.ssh/id_ecdsa_deploy /home/vagrant/.ssh/id_ecdsa_deploy
chown vagrant:vagrant /home/vagrant/.ssh/id_ecdsa_deploy
# eval $(ssh-agent -s)
# ssh-add /home/vagrant/.ssh/id_ecdsa_local



echo "-------------------------------------------------------------------"
echo "Fin des installations du fichier serveur_deploiement_requirements.sh"
echo "-------------------------------------------------------------------"
