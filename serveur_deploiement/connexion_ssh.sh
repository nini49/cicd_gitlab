#!/bin/bash

adresse_ip_du_serveur=10.0.0.11
# username_du_compte_qui_est_dans_le_serveur=vagrant
# mot_de_passe_du_username=vagrant
# nombre_serveur=5

# nom_variable_adresse_ip_du_serveur=${adresse_ip_du_serveur//./_}
# echo $nom_variable_adresse_ip_du_serveur


# glab variable set SSH_PRIVATE_10_0_0_11 < cd/serveur_deploiement/.vagrant/machines/deploiement1/virtualbox/private_key 

# nom_user=deploy

echo "Creer clé ssh local"
mkdir /home/$nom_user/.ssh
nom_variable_adresse_ip_du_serveur=${adresse_ip_du_serveur//./_}
ssh-keygen -q -t ecdsa -N '' -f /home/$USER/.ssh/id_ecdsa_$nom_variable_adresse_ip_du_serveur -b 521
cat /home/$USER/.ssh/id_ecdsa_$nom_variable_adresse_ip_du_serveur.pub | tee /home/$USER/.ssh/authorized_keys
# chown -R $nom_user:$nom_user /home/$nom_user/.ssh
sshpass -p motdepasse ssh-copy-id -i /home/$USER/.ssh/id_ecdsa_$nom_variable_adresse_ip_du_serveur -o StrictHostKeyChecking=no deploy@$adresse_ip_du_serveur

