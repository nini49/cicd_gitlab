#!/bin/bash

adresse_ip_du_serveur=10.0.0.1
username_du_compte_qui_est_dans_le_serveur=vagrant
mot_de_passe_du_username=vagrant
nombre_serveur=1

echo "Creation de la variable 'NOMBRE_SERVEUR' pour le 'Vagrantfile'"
export NOMBRE_SERVEUR=$nombre_serveur

echo "Suppression du serveur vagrant pour le deploiement"
cd /home/$USER/gitlab/cicd_gitlab/serveur_deploiement
vagrant destroy -f \
&& echo Suppression des serveurs terminés

echo "Suppression des clés ssh stockées dans l'agent ssh"
ssh-add -D \
&& ssh-add -l

echo "Suppression dossier cle_ssh"
rm -rf /home/$USER/gitlab/cicd_gitlab/serveur_deploiement/cle_ssh

echo "Suppression des cles ssh pc_portable (local)"
rm -rf /home/$USER/.ssh/id_ecdsa_local /home/$USER/.ssh/id_ecdsa_local.pub

START=1
END=$nombre_serveur

for ((c=$START; c<=$END; c++))
    do 
        echo "Supression variable - clé privé ssh dans gitlab"
        nom_variable_adresse_ip_du_serveur=${adresse_ip_du_serveur//./_}
        glab variable delete SSH_PRIVATE_$nom_variable_adresse_ip_du_serveur$c

        echo "Supression variable - user ssh dans gitlab"
        glab variable delete SSH_USER_$nom_variable_adresse_ip_du_serveur$c

        # echo "Supression variable - user ssh dans gitlab"
        # glab variable delete SSH_PSW_$nom_variable_adresse_ip_du_serveur$c


    done


echo "-------------------------------------------------------------------"
echo "Fin du lancement du fichier suppression_serveur_deploiement.sh"
echo "-------------------------------------------------------------------"
